var state = {};

var html = document.querySelector("html");
var dialog = document.querySelector("dialog");
var form = document.querySelector("#rsvp-form");
var gallery = document.querySelector("#gallery");
var submitButton = document.querySelector("#rsvp-form-submit");

for (var i = 1; i <= 8; i++) {
  var img = document.createElement("img");
  img.src = `img/${i}.jpg`;
  img.tabIndex = 0;

  function onClickImage() {
    html.classList.add("dialog-open");
    dialog.querySelector("img").src = this.src;
    dialog.querySelector("img").classList.remove("hidden");
    dialog.querySelector("p").remove();
  }

  img.onclick = onClickImage;
  img.onkeydown = function (ev) {
    if (ev.key === "Enter") {
      onClickImage.call(this);
    }
  };

  gallery.appendChild(img);
}

function onSubmit(ev) {
  ev.preventDefault();
  var FD = new FormData(form);
  var XHR = new XMLHttpRequest();
  XHR.open("POST", "https://www.10yea.rs/post.php");
  XHR.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  XHR.send(FD);

  XHR.addEventListener("load", (event) => {
    html.classList.add("rsvp-success");
    html.classList.add("dialog-open");
    document.querySelector("#rsvp-form-submit").disabled = true;
    dialog.querySelector("img").classList.add("hidden");
    dialog.appendChild(document.createElement("p")).textContent =
      "Thank you for your RSVP! We'll send more details to you by email. See you soon!";
  });

  XHR.addEventListener("error", (event) => {
    alert("Something went wrong! Try again later or contact Lucas.");
  });
}

submitButton.addEventListener("click", onSubmit);

function closeDialog() {
  html.classList.remove("dialog-open");
}

document.addEventListener("keydown", function (ev) {
  if (ev.key === "Escape") {
    closeDialog();
  }
});

function onChangeCanAttend(ev) {}
